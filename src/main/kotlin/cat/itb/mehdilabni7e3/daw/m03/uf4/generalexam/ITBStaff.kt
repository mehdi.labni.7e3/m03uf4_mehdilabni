package cat.itb.mehdilabni7e3.daw.m03.uf4.generalexam

open class Persona(val dni: String,
                    val nom: String) {
    open fun mostrarInformacio(){
        println("$dni : $nom")
    }

}

class PAS(dni: String,
          nom: String,
          val posicio: String): Persona(dni,nom){
    override fun mostrarInformacio(){
        println("$dni: $nom ($posicio)")
    }

}

class Professor(dni: String, nom: String, val moduls: List<String>): Persona(dni,nom){
    override fun mostrarInformacio(){
        println("$dni: $nom $moduls")
    }
}

fun mostrarPAS(personal: List<Persona>){
    println("# Personal d'administració i serveis:")
    for (i in personal){
        if (i is PAS){
            i.mostrarInformacio()
        }
    }
    println("")
}

fun mostrarProfessors(personal: List<Persona>){
    println("# Els professors:")
    for (i in personal){
        if (i is Professor){
            i.mostrarInformacio()
        }
    }

}

fun main() {
    val personal = mutableListOf<Persona>()

    personal.add(PAS("10101", "Elisenda", "Secretaria"))
    personal.add(PAS("34010", "Ismael", "Secretaria"))
    personal.add(PAS("24017", "Montse", "Secretaria"))
    personal.add(Professor("503854", "Marc", mutableListOf("M03", "M05")))
    personal.add(Professor("503850", "Javi", mutableListOf("M03")))
    personal.add(PAS("59270", "Ester", "Consergeria"))
    personal.add(PAS("32740", "Artur", "Consergeria"))
    personal.add(Professor("591040", "David", mutableListOf("M01","M08")))
    personal.add(Professor("503695", "Francesc", mutableListOf("M02")))
    personal.add(Professor("503648", "Alicia", mutableListOf("M04")))

    personal.forEach(){it.mostrarInformacio()}
    println()

    mostrarPAS(personal)
    mostrarProfessors(personal)

}