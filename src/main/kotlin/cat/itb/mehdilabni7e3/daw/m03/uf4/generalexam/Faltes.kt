package cat.itb.mehdilabni7e3.daw.m03.uf4.generalexam

import java.time.LocalDate
import java.util.*

fun justificar(date: LocalDate, listFates: List<Falta>){
    for (i in listFates){
        if (i.dia == date){
            i.tipus = tipusFaltes.J
        }
    }
}

fun countFaltes(uf: ufM3,listFates: List<Falta>): String{
    var faltes = 0
    for (i in listFates){
        if (i.uf == uf ) {
            faltes += 1
        }
    }

    return "$uf: $faltes Faltes"
}


fun main() {
    val faltes = mutableListOf<Falta>()
    faltes.add(Falta(LocalDate.now(), ufM3.M3UF4))
    faltes.add(Falta(ufM3.M3UF4, tipusFaltes.V))
    faltes.forEach(){
        println(it)
    }

    println()

    justificar(LocalDate.now(),faltes)
    faltes.forEach(){
        println(it)
    }

    println()

    println(countFaltes(ufM3.M3UF1, faltes))
    println(countFaltes(ufM3.M3UF2, faltes))
    println(countFaltes(ufM3.M3UF3, faltes))
    println(countFaltes(ufM3.M3UF4, faltes))
    println(countFaltes(ufM3.M3UF5, faltes))
    println(countFaltes(ufM3.M3UF6, faltes))

}