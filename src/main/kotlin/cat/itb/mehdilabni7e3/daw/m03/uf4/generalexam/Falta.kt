package cat.itb.mehdilabni7e3.daw.m03.uf4.generalexam

import java.time.LocalDate

class Falta(val dia: LocalDate, val uf: ufM3) {
    var tipus: tipusFaltes

    init {
    tipus = tipusFaltes.F
    }

    constructor(uf: ufM3,  tipus: tipusFaltes) : this(LocalDate.now() ,uf){
        this.tipus = tipus
    }

    override fun toString(): String {
        return "$uf: $dia $tipus"
    }

}

enum class ufM3 (){
    M3UF1, M3UF2, M3UF3, M3UF4, M3UF5, M3UF6
}

enum class tipusFaltes (){
    F, J, V ,R
}


